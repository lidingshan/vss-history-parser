require 'rspec'
require './vss_history_report_parser'

describe VSSHistoryReportParser do 

	before(:each) do
		file_path = './spec/test_data/test_history_report.txt'
		@output_path = './spec/test_data/test_output.csv'
		@parser = VSSHistoryReportParser.new(file_path, @output_path)
	end

	it 'should be able to open input file' do 
		expect(@parser).to be_truthy
		expect(@parser).to be_instance_of(VSSHistoryReportParser)
	end

	it 'should be able to know if it is a new history record' do
		input_line = "*****************  test.txt  *****************"
		expect(@parser.new_record?(input_line)).to be true
	end

	it 'should be able to skip non record content' do
		input_line = '$/'
		expect(@parser.new_record?(input_line)).to be false		
	end

	it 'should be able to get updated file name' do
		input_line = "*****************  S2331_wosin.sas  *****************"

		expected = 'S2331_wosin.sas'
		actual = @parser.get_updated_file_name(input_line)

		expect(actual).to eq(expected)
	end

	it 'should be able to get the history record checkin date' do
		input_line = 'User: Admin           Date:  1/08/14  Time:  9:55a'
		
		expected = '1/08/14'
		actual = @parser.get_updated_date(input_line)

		expect(actual).to eq(expected)
	end

	it 'should be able to get the checked in folder' do
		input_line = 'Checked in $/Web/PMS/WOS'

		expected = '$/Web/PMS/WOS'
		actual = @parser.get_updated_folder(input_line)

		expect(actual).to eq(expected)
	end

	it 'should be able to read multiple history records' do
		histories = @parser.get_histories

		expect(histories[0][:file]).to eq('S2331_wosin.sas')
		expect(histories[0][:path]).to eq('$/Web/PMS/WOS')
		expect(histories[0][:date]).to eq('1/08/14')
		expect(histories[0][:action]).to eq('Checked in')

		expect(histories[1][:file]).to eq('S1008_NZ_cat.sas')
		expect(histories[1][:path]).to eq('$/ReInsurance/Reindeer/NewZealand')
		expect(histories[1][:date]).to eq('31/07/14')
		expect(histories[1][:action]).to eq('Checked in')

		expect(histories[2][:file]).to eq('AssemblyInfo.cs')
		expect(histories[2][:path]).to eq('Properties')
		expect(histories[2][:date]).to eq('11/06/14')
		expect(histories[2][:action]).to eq('Added')

		expect(histories[3][:file]).to eq('ImpactTool.exe')
		expect(histories[3][:path]).to eq('bin')
		expect(histories[3][:date]).to eq('11/06/14')
		expect(histories[3][:action]).to eq('Deleted')
	end

	it 'should be able to export as a CSV file' do
		@parser.get_histories
		@parser.export
		expect(File.exist?(@output_path)).to be true
	end

	it 'should be able to get file name without extention' do 
		input_line = "*****************  samplefile  *****************"

		expected = 'samplefile'
		actual = @parser.get_updated_file_name(input_line)

		expect(actual).to eq(expected)		
	end

	it 'should be able to get file name include space' do 
		input_line = "*****************  sample file  *****************"

		expected = 'sample file'
		actual = @parser.get_updated_file_name(input_line)

		expect(actual).to eq(expected)		
	end

	it 'should be able to indicate checkin action' do
		input_line = 'Checked in $/Web/PMS/WOS'
		expected_action = "Checked in"
		actual = @parser.get_update_action(input_line)

		expect(actual).to eq(expected_action)
	end

	it 'should be able to indicate add action' do
		input_line = 'Added samplefile'
		expected_action = "Added"
		actual = @parser.get_update_action(input_line)

		expect(actual).to eq(expected_action)
	end

	it 'should be able to indicate delete action' do
		input_line = 'Deleted samplefile'
		expected_action = "Deleted"
		actual = @parser.get_update_action(input_line)

		expect(actual).to eq(expected_action)
	end	
end