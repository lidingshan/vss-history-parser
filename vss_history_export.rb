require './vss_history_report_parser'

history_log = ARGV[0]
output_file = ARGV[1]

puts 'Start to parse %s and export to %s' % [history_log, output_file]

parser = VSSHistoryReportParser.new(history_log, output_file)
parser.get_histories
parser.export

puts 'Export completed'