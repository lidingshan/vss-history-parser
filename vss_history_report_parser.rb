
class VSSHistoryReportParser

	def initialize(file, output_file)
		@report_file = File.new(file)
		@output_path = output_file

		@histories = []
	end

	def get_updated_file_name(line)
		updated_file_name = line[/([A-Za-z0-9|\-|_]+)[\s]*(\.){0,1}[A-Za-z0-9]+/]
		updated_file_name
	end

	def get_updated_date(line)
		updated_date = line[/[0-9]{1,2}(\/)[0-9]{1,2}(\/)[0-9]{2}/]
		updated_date
	end

	def get_update_action(line)
		update_action = line[/^(Checked in){1}|(Added){1}|(Deleted){1}/]
		update_action
	end

	def get_updated_folder(line)
		updated_folder = line[/(\$){1}([A-Za-z0-9\/\-_\.| ])*$/]
		updated_folder	
	end

	def new_record?(line)
		prefix_new_record = line[/^(\*){17}/]
		if prefix_new_record.nil?
			false
		else
			true
		end
	end

	def get_histories
		until @report_file.eof? do
			line = @report_file.readline

			if new_record?(line)
				history = {}
		
				updated_file_name = get_updated_file_name(line)
				if updated_file_name.nil? 
					next
				end

				updated_date = get_updated_date(@report_file.readline)
				
				next_line = @report_file.readline
				update_action = get_update_action(next_line)

				updated_folder = ''
				case update_action
				when 'Checked in'
					updated_folder = get_updated_folder(next_line)
				else
					updated_folder = updated_file_name
					updated_file_name = next_line[/[^(#{update_action})][\d|\w|\s|\.|\-]+$/].strip
				end

				history = {
					:file => updated_file_name, 
					:path => updated_folder,
					:date => updated_date,
					:action => update_action
				}

				@histories.push(history)
			end
		end
		
		return @histories
	end

	def export
		File.open(@output_path, 'w') do |f|
			f.puts 'File, Path, Checkedin, Added, Deleted'

			summarize_history.each { |key, line|
				one_line = '%s, %s, %s, %s, %s' % [key, line[:path], line["Checked in"], line["Added"], line["Deleted"]]
				f.puts one_line
			}
		end
	end

	def summarize_history
		lines = {}

		@histories.each{ |history|
			filename = history[:file]
			action = history[:action]

			if lines.has_key?(filename)
				if lines[filename][action]
					lines[filename][action] += 1
				else
					lines[filename][action] = 1
				end
			else
				line = {}
				line[action] = 1
				line[:path] = history[:path]

				lines[filename] = line
			end
		}

		lines
	end
end